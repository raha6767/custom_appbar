library flutter_custom_appbar;

import 'package:flutter/material.dart';

class CustomAppbar extends StatefulWidget implements PreferredSizeWidget {
  final String title;
  final bool backButton;
  final Color? background;
  final Color? cartIcon;
  final Color? sortIcon;
  final Color? badgeContentColor;
  final PreferredSizeWidget? bottom;
  final Function()? backFromScreen;
  @override
  Size get preferredSize => bottom != null
      ? const Size.fromHeight(120.0)
      : const Size.fromHeight(60.0);

  const CustomAppbar(
      {Key? key,
      required this.title,
      required this.backButton,
      this.background,
      this.cartIcon,
      this.sortIcon,
      this.badgeContentColor,
      this.bottom,
      this.backFromScreen})
      : super(key: key);

  @override
  State<CustomAppbar> createState() => _CustomAppbarState();
}

class _CustomAppbarState extends State<CustomAppbar> {
  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: widget.background ?? Colors.white,
      elevation: 0,
      bottom: widget.bottom,
      centerTitle: true,
      leading: widget.backButton
          ? IconButton(
              onPressed: () {},
              icon: const Icon(
                Icons.arrow_back,
                size: 24,
                color: Colors.blueGrey,
              ))
          : Builder(
              builder: (context) => GestureDetector(
                  onTap: () {
                    Scaffold.of(context).openDrawer();
                  },
                  child: Icon(
                    Icons.sort,
                    color: widget.sortIcon ?? Colors.blueGrey,
                    size: 30,
                  )),
            ),
      title: Text(
        widget.title,
      ),
      actions: [
        Padding(
          padding: const EdgeInsets.only(left: 0),
          child: Stack(
            alignment: Alignment.center,
            children: [
              IconButton(
                  onPressed: () {},
                  icon: const Icon(
                    Icons.star,
                    color: Colors.green,
                    size: 30,
                  )),
              Container(
                  height: 18,
                  width: 18,
                  margin: const EdgeInsets.only(top: 15.0, left: 15.0),
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: widget.badgeContentColor ?? Colors.red,
                  ),
                  child: const Text(
                    "15",
                    textAlign: TextAlign.center,
                  ))
            ],
          ),
        ),
      ],
    );
  }
}
