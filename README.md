<!-- 
This README describes the package. If you publish this package to pub.dev,
this README's contents appear on the landing page for your package.

For information about how to write a good package README, see the guide for
[writing package pages](https://dart.dev/guides/libraries/writing-package-pages). 

For general information about developing packages, see the Dart guide for
[creating packages](https://dart.dev/guides/libraries/create-library-packages)
and the Flutter guide for
[developing packages and plugins](https://flutter.dev/developing-packages). 
-->

TODO: A new Flutter package with a customized Appbar.

## Features

TODO: My Package give you custom app bar

## Getting started

TODO: A new Flutter package with a customized Appbar.

## Usage

TODO: A new Flutter package with a customized Appbar.

```dart
class Home extends StatelessWidget {
  const Home({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const CustomAppbar(title: "Home", backButton: false,),
      body: Container(),
    );
  }
}
```

## Additional information

TODO: A new Flutter package with a customized Appbar.
